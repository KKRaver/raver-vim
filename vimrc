call plug#begin()
Plug 'scrooloose/nerdtree'
Plug 'mattn/emmet-vim'
Plug 'arcticicestudio/nord-vim'
Plug 'arcticicestudio/nord-vim'
Plug 'jiangmiao/auto-pairs'
Plug 'vim-airline/vim-airline'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-fugitive'
Plug 'zxqfl/tabnine-vim'
Plug 'nvie/vim-flake8'
Plug 'tell-k/vim-autopep8'
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }
call plug#end()
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
map <C-n> :NERDTreeToggle<CR>
set nu
syntax on
set history=999
set undolevels=999
set autoread
set hlsearch
set incsearch
set ignorecase smartcase
set t_Co=256
set cursorline
set title
set showmatch
set autoindent smartindent
set spell
let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_SR= "\<Esc>]50;CursorShape=1\x7"
let &t_EI = "\<Esc>]50;CursorShape=1\x7"
let NERDTreeHighlightCursorline=1
colorscheme gruvbox
set background=dark
autocmd Filetype css setlocal tabstop=2
autocmd Filetype ts setlocal tabstop=2
autocmd Filetype html setlocal tabstop=2
autocmd FileType python noremap <buffer> <F8> :call Autopep8()<CR>